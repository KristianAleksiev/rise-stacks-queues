﻿namespace UniqueElements
{
    public class UniqueElements
    {
        static void Main(string[] args)
        {   
            
            List<string> inputList = new List<string>();
            Console.WriteLine("Please enter the length of the list");
            int length = int.Parse(Console.ReadLine());

            Console.WriteLine("Please enter the elements of the list:");
            for (int i = 0; i < length; i++)
            {
            inputList.Add(Console.ReadLine());
            }

            List<string> unique = GetUniqueElements(inputList);

            Console.WriteLine("The unique elements of the list are: ");
            foreach (string uniqueElement in unique)
            {
            Console.Write(uniqueElement + " ");
            }

            Console.Read();
        }

        public static List<string> GetUniqueElements(List<string> inputList) 
        {
            List<string> result = new List<string>();
            result = inputList.Distinct().ToList();

            return result;
        }
    }
}