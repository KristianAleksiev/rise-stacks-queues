﻿namespace ExpressionExpansion
{
    public class ExpressionExpansion
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter an expression:");
            string expression = Console.ReadLine();

            string result = ExpExpansion(expression);
            Console.WriteLine(result);

        }

        static string ExpExpansion(string expression) 
        {
            string unpacked = "";

            foreach (char currentChar in expression)
            {
                if (char.IsDigit(currentChar))
                {
                    int num = int.Parse(currentChar.ToString());
                    unpacked += new string(unpacked.Last(), num - 1);
                }

                else
                {
                    unpacked += currentChar;
                }
            }
            return unpacked;
        }
    }
}